﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio
{
    public class Estudiante : Persona
    {
        private string _carrera;
        private string _materia;
        private int _nota;
        private bool _estado;

        public Estudiante()
        {

        }

        public Estudiante(int codigo, int cedula, string nombre, string carrera, string materia, int nota, bool estado)
        : base(codigo, cedula, nombre)
        {
            _carrera = carrera;
            _materia = materia;
            _nota = nota;
            _estado = estado;
        }

        public bool estado
        {
            get { return _estado; }
            set { _estado = value; }
        }


        public int nota
        {
            get { return _nota; }
            set { _nota = value; }
        }


        public string materia
        {
            get { return _materia; }
            set { _materia = value; }
        }


        public string carrera
        {
            get { return _carrera; }
            set { _carrera = value; }
        }
    }
}
