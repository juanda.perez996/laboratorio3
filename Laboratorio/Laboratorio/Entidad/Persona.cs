﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio
{
    public class Persona
    {
        private int _codigo;
        private int _cedula;
        private string _nombre;

        public Persona()
        {
        }

        public Persona(int codigo, int cedula, string nombre)
        {
            _codigo = codigo;
            _cedula = cedula;
            _nombre = nombre;
        }

        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }


        public int cedula
        {
            get { return _cedula; }
            set { _cedula = value; }
        }

        public int codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

            
    }
}
