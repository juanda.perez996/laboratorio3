﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio
{
    public class SingletonEstudiante
    {
        private static SingletonEstudiante instancia;
        LinkedList<Estudiante> linkEstudiante = new LinkedList<Estudiante>();
        Estudiante estudiante;
        private SingletonEstudiante()
        {

        }

        public static SingletonEstudiante obtenerInstancia()
        {
            if (instancia == null)
            {
                instancia = new SingletonEstudiante();
            }
            return instancia;
        }

        public void agregarEstudiante(int codigo, int cedula, string nombre, string carrera, string materia, int nota, bool estado)
        {
            estudiante = new Estudiante(codigo, cedula, nombre, carrera, materia, nota, estado);
            linkEstudiante.AddLast(estudiante);
        }

        public LinkedList<Estudiante> obtenerEstudiantes() 
        {
            return linkEstudiante;
        }

        public void borrarEstudiante(Estudiante est) 
        {
            linkEstudiante.Remove(linkEstudiante.Find(est));
        }

        public void actualizarEstudiante(int identificador, int codigo, int cedula, string nombre, string carrera, string materia, int nota, bool estado)
        {
            foreach (Estudiante est in linkEstudiante)
            {
                if (est.cedula == identificador)
                {
                    Estudiante updated = new Estudiante(codigo, cedula, nombre, carrera, materia, nota, estado);
                    linkEstudiante.Find(est).Value = updated;
                }
            }
        }   
    }
}
