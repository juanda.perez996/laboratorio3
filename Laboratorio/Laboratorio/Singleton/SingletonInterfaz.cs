﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio
{
    public class SingletonInterfaz
    {
        private static SingletonInterfaz instancia;
        
        Actualizar acutlizarUI;

        private SingletonInterfaz()
        {

        }

        public static SingletonInterfaz obtenerInstancia()
        {
            if (instancia == null)
            {
                instancia = new SingletonInterfaz();
            }
            return instancia;
        }

        public void agregarEstudiante(SingletonEstudiante singEst) 
        {
            Agregar agregarUI = new Agregar(singEst);
            agregarUI.Show();
        }

        public void actualizarEstudiante(Estudiante est, SingletonEstudiante singEst)
        {
            Actualizar actualizarUI = new Actualizar(est, singEst);
            actualizarUI.Show();
        }
    }
}
