﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio
{
    public partial class Lobby : Form
    {
        SingletonEstudiante crudEstudiante = SingletonEstudiante.obtenerInstancia();
        SingletonInterfaz UIestudiantes = SingletonInterfaz.obtenerInstancia();
        public Lobby()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Lobby_Load(object sender, EventArgs e)
        {
            crudEstudiante.agregarEstudiante(1,207500069,"JuanDaniel","ISW","Programación III",100,true);
            cargarEstudiantes();
        }

        private void cargarEstudiantes() 
        {
            dgvEstudiantes.Rows.Clear();
            foreach (var est in crudEstudiante.obtenerEstudiantes())
            {
                if (est.estado = true)
	            {
                    int newRow = dgvEstudiantes.Rows.Add();
                    dgvEstudiantes.Rows[newRow].Cells[0].Value = est.codigo;
                    dgvEstudiantes.Rows[newRow].Cells[1].Value = est.cedula;
                    dgvEstudiantes.Rows[newRow].Cells[2].Value = est.nombre;
                    dgvEstudiantes.Rows[newRow].Cells[3].Value = est.carrera;
                    dgvEstudiantes.Rows[newRow].Cells[4].Value = est.materia;
                    dgvEstudiantes.Rows[newRow].Cells[5].Value = est.nota;
                    dgvEstudiantes.Rows[newRow].Cells[6].Value = est.estado;
	            }
                

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UIestudiantes.agregarEstudiante(crudEstudiante);
            cargarEstudiantes();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dgvEstudiantes.SelectedRows.Count>-1)
            {
                UIestudiantes.actualizarEstudiante(crudEstudiante.obtenerEstudiantes().ElementAt(dgvEstudiantes.CurrentRow.Index),crudEstudiante);
            }
            cargarEstudiantes();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgvEstudiantes.SelectedRows.Count > -1)
            {
                crudEstudiante.borrarEstudiante(crudEstudiante.obtenerEstudiantes().ElementAt(dgvEstudiantes.CurrentRow.Index));
            }
            
            cargarEstudiantes();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            cargarEstudiantes();
        }
    }
}
