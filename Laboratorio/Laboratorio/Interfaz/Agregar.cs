﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio
{
    public partial class Agregar : Form
    {
        SingletonEstudiante singEst;
        public Agregar(SingletonEstudiante singEst)
        {
            InitializeComponent();
            this.singEst = singEst;
        }

        private void Agregar_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cmbCarrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCarrera.SelectedIndex == 0)
            {
                cmbMateria.Items.Add("Programación III");
                cmbMateria.Items.Add("Base de datos II");
            }
            else if(cmbCarrera.SelectedIndex == 1)
            {
                cmbMateria.Items.Add("Writing II");
                cmbMateria.Items.Add("Finance II");
            }
            else if (cmbCarrera.SelectedIndex == 2)
            {
                cmbMateria.Items.Add("Organica I");
                cmbMateria.Items.Add("Fisica III");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(string.IsNullOrEmpty(txtCodigo.Text) || string.IsNullOrEmpty(txtCedula.Text) || string.IsNullOrEmpty(txtNombre.Text)))
                {
                    bool estado = false;
                    if (int.Parse(txtNota.Text) >= 70)
                    {
                        estado = true;
                    }
                    singEst.agregarEstudiante(int.Parse(txtCodigo.Text), int.Parse(txtCedula.Text), txtNombre.Text, cmbCarrera.Text, cmbMateria.Text, int.Parse(txtNota.Text), estado);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Datos incompletos");
                }
            }
            catch (Exception x)
            {

                MessageBox.Show(x.Message);
            }
            
            
        }
    }
}
