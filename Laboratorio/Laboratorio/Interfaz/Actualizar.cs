﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio
{
    public partial class Actualizar : Form
    {
        Estudiante est = new Estudiante();
        SingletonEstudiante singEst;
        public Actualizar(Estudiante est,SingletonEstudiante singEst)
        {
            InitializeComponent();
            this.est = est;
            this.singEst = singEst;
        }

        private void Actualizar_Load(object sender, EventArgs e)
        {
            txtCodigo.Text = est.codigo.ToString();
            txtCedula.Text = est.cedula.ToString();
            txtNombre.Text = est.nombre;
            txtNota.Text = est.nota.ToString();
            if (est.carrera.Equals("ISW"))
            {
                cmbCarrera.SelectedIndex = 0;
            }
            else if (est.carrera.Equals("ILE")) 
            {
                cmbCarrera.SelectedIndex = 2;
            }
            else if (est.carrera.Equals("IGA"))
            {
                cmbCarrera.SelectedIndex = 3;
            }
            if (cmbCarrera.SelectedIndex == 0)
            {
                cmbMateria.Items.Add("Programación III");
                cmbMateria.Items.Add("Base de datos II");
                if (est.materia.Equals("Programación III"))
                {
                    cmbMateria.SelectedIndex = 0;
                }
                else if(est.materia.Equals("Base de datos II")) 
                {
                    cmbMateria.SelectedIndex = 1;
                }
            }
            else if (cmbCarrera.SelectedIndex == 1)
            {
                cmbMateria.Items.Add("Writing II");
                cmbMateria.Items.Add("Finance II");
                if (est.materia.Equals("Writing II"))
                {
                    cmbMateria.SelectedIndex = 0;
                }
                else if (est.materia.Equals("Finance II"))
                {
                    cmbMateria.SelectedIndex = 1;
                }
            }
            else if (cmbCarrera.SelectedIndex == 2)
            {
                cmbMateria.Items.Add("Organica I");
                cmbMateria.Items.Add("Fisica III");
                if (est.materia.Equals("Organica I"))
                {
                    cmbMateria.SelectedIndex = 0;
                }
                else if (est.materia.Equals("Fisica III"))
                {
                    cmbMateria.SelectedIndex = 1;
                }
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbCarrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCarrera.SelectedIndex == 0)
            {
                cmbMateria.Items.Add("Programación III");
                cmbMateria.Items.Add("Base de datos II");
            }
            else if (cmbCarrera.SelectedIndex == 1)
            {
                cmbMateria.Items.Add("Writing II");
                cmbMateria.Items.Add("Finance II");
            }
            else if (cmbCarrera.SelectedIndex == 2)
            {
                cmbMateria.Items.Add("Organica I");
                cmbMateria.Items.Add("Fisica III");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(string.IsNullOrEmpty(txtCodigo.Text) || string.IsNullOrEmpty(txtCedula.Text) || string.IsNullOrEmpty(txtNombre.Text)))
                {
                    bool estado = false;
                    if (int.Parse(txtNota.Text) >= 70)
                    {
                        estado = true;
                    }
                    singEst.actualizarEstudiante(est.cedula, int.Parse(txtCodigo.Text), int.Parse(txtCedula.Text), txtNombre.Text, cmbCarrera.Text, cmbMateria.Text, int.Parse(txtNota.Text), estado);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Datos incompletos");
                }
            }
            catch (Exception x)
            {

                MessageBox.Show(x.Message);
            }
            
        }
    }
}
